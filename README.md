# springbootTest

#### 项目介绍
springboot微服务框架小试身手

#### 软件架构
软件架构说明
1.rabbitmq

2.redis

3.restful web

4.spring boot调度

5.mybatis

6.spring boot 异常处理

7.spring boot 拦截器

8.spring boot 过滤器

9.spring boot 文件处理上传，下载

10.spring boot 国际化

11.spring boot web socket

12.spring Boot Security

13.spring boot 启用HTTPS

其他后期补充...

1.test 下设计模式测试

![输入图片说明](https://images.gitee.com/uploads/images/2018/1203/153157_d8f1d189_637068.png "搜狗截图20181203153131.png")


#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)