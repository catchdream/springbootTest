package com.designmode.adapterpattern;

/**
 * 创建实现AdvancedMediaPlayer接口的具体类
 * 
 * @author tao
 *
 */
public class VlcPlayer implements AdvancedMediaPlayer {

	@Override
	public void playVlc(String fileName) {
		System.out.println("Playing vlc file. Name: " + fileName);
	}

	@Override
	public void playMp4(String fileName) {
		// do nothing
	}

}
