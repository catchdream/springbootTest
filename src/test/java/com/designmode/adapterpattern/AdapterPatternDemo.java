package com.designmode.adapterpattern;

/**
 * 适配器模式作为两个不兼容接口之间的桥梁。 这种类型的设计模式属于结构模式，因为该模式组合了两个独立接口。
 * 
 * @author tao
 *
 */
public class AdapterPatternDemo {
	public static void main(String[] args) {
		AudioPlayer audioPlayer = new AudioPlayer();

		audioPlayer.play("mp3", "beyond the horizon.mp3");
		audioPlayer.play("mp4", "alone.mp4");
		audioPlayer.play("vlc", "far far away.vlc");
		audioPlayer.play("avi", "mind me.avi");
	}
}
