package com.designmode.adapterpattern;

public interface MediaPlayer {
	public void play(String audioType, String fileName);
}
