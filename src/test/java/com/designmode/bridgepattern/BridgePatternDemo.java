package com.designmode.bridgepattern;

/**
 * 桥接模式测试程序
 * 
 * @author tao
 *
 */
public class BridgePatternDemo {
	public static void main(String[] args) {
		BShape redCircle = new BCircle(100, 100, 10, new RedCircle());
		BShape greenCircle = new BCircle(100, 100, 10, new GreenCircle());

		redCircle.draw();
		greenCircle.draw();
	}
}
