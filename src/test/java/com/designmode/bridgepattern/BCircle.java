package com.designmode.bridgepattern;

/**
 * 创建实现Shape接口的具体类。
 * 
 * @author tao
 *
 */
public class BCircle extends BShape {
	private int x, y, radius;

	public BCircle(int x, int y, int radius, DrawAPI drawAPI) {
		super(drawAPI);
		this.x = x;
		this.y = y;
		this.radius = radius;
	}

	public void draw() {
		drawAPI.drawCircle(radius, x, y);
	}
}
