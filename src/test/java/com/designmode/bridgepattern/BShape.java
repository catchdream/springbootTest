package com.designmode.bridgepattern;

/**
 * 使用DrawAPI接口创建一个抽象类Shape。
 * 
 * @author tao
 *
 */
public abstract class BShape {
	protected DrawAPI drawAPI;

	protected BShape(DrawAPI drawAPI) {
		this.drawAPI = drawAPI;
	}

	public abstract void draw();
}
