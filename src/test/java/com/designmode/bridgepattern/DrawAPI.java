package com.designmode.bridgepattern;

public interface DrawAPI {
	public void drawCircle(int radius, int x, int y);
}
