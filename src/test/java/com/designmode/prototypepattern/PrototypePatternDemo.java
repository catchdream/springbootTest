package com.designmode.prototypepattern;

/**
 * **原型模式指在创建重复对象的同时保持性能。***************************
 * **这种类型的设计模式属于创建模式，因为此模式提供了创建对象的最佳方法之一。** ***这个模式涉及实现一个原型接口，它只创建当前对象的克隆。***
 * **##原型模式测试程序。##***
 * 
 * @author tao
 *
 */
public class PrototypePatternDemo {
	public static void main(String[] args) {
		ShapeCache.loadCache();

		PShape clonedShape = (PShape) ShapeCache.getShape("1");
		System.out.println("Shape : " + clonedShape.getType());

		PShape clonedShape2 = (PShape) ShapeCache.getShape("2");
		System.out.println("Shape : " + clonedShape2.getType());

		PShape clonedShape3 = (PShape) ShapeCache.getShape("3");
		System.out.println("Shape : " + clonedShape3.getType());
	}
}
