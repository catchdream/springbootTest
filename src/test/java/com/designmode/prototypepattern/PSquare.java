package com.designmode.prototypepattern;

/**
 * 实现抽象类2
 * 
 * @author tao
 *
 */
public class PSquare extends PShape {
	public PSquare() {
		type = "Square";
	}

	@Override
	public void draw() {
		System.out.println("Inside Square::draw() method.");
	}
}
