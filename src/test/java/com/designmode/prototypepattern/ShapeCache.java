package com.designmode.prototypepattern;

import java.util.Hashtable;

/**
 * 创建一个类来获取具体的类，并将它们存储在Hashtable中。
 * 
 * @author tao
 *
 */
public class ShapeCache {
	private static Hashtable<String, PShape> shapeMap = new Hashtable<String, PShape>();

	public static PShape getShape(String shapeId) {
		PShape cachedShape = shapeMap.get(shapeId);
		return (PShape) cachedShape.clone();
	}

	// for each shape run database query and create shape
	// shapeMap.put(shapeKey, shape);
	// for example, we are adding three shapes

	public static void loadCache() {
		PCircle circle = new PCircle();
		circle.setId("1");
		shapeMap.put(circle.getId(), circle);

		PSquare square = new PSquare();
		square.setId("2");
		shapeMap.put(square.getId(), square);

		PRectangle rectangle = new PRectangle();
		rectangle.setId("3");
		shapeMap.put(rectangle.getId(), rectangle);
	}
}
