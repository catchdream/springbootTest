package com.designmode.builderpattern;

/**
 * 抽象类实现食品类2
 * 
 * @author tao
 *
 */
public abstract class ColdDrink implements Item {
	@Override
	public Packing packing() {
		return new Bottle();
	}

	@Override
	public abstract float price();
}
