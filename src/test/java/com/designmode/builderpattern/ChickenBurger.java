package com.designmode.builderpattern;

/**
 * 抽象汉堡类的具体实现2
 * 
 * @author tao
 *
 */
public class ChickenBurger extends Burger {

	@Override
	public float price() {
		return 50.5f;
	}

	@Override
	public String name() {
		return "Chicken Burger";
	}

}
