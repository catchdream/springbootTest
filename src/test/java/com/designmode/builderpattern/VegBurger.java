package com.designmode.builderpattern;

/**
 * 抽象汉堡类的具体实现类1
 * 
 * @author tao
 *
 */
public class VegBurger extends Burger {

	@Override
	public float price() {
		return 25.0f;
	}

	@Override
	public String name() {
		return "Veg Burger";
	}

}
