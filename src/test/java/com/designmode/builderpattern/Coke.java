package com.designmode.builderpattern;

/**
 * 抽象冷饮类的具体实现1
 * 
 * @author tao
 *
 */
public class Coke extends ColdDrink {
	@Override
	public float price() {
		return 30.0f;
	}

	@Override
	public String name() {
		return "Coke";
	}
}
