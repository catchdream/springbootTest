package com.designmode.builderpattern;

/**
 * 抽象冷饮类的具体实现2
 * 
 * @author tao
 *
 */
public class Pepsi extends ColdDrink {
	@Override
	public float price() {
		return 35.0f;
	}

	@Override
	public String name() {
		return "Pepsi";
	}
}
