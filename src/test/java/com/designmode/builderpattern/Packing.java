package com.designmode.builderpattern;

/**
 * 包装类
 * 
 * @author tao
 *
 */
public interface Packing {
	public String pack();
}
