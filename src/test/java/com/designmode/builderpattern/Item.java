package com.designmode.builderpattern;

/**
 * 食品类
 * 
 * @author tao
 *
 */
public interface Item {
	public String name();

	public Packing packing();

	public float price();
}
