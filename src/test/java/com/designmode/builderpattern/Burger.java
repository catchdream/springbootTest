package com.designmode.builderpattern;

/**
 * 抽象类实现食品类1
 * 
 * @author tao
 *
 */
public abstract class Burger implements Item {
	@Override
	public Packing packing() {
		return new Wrapper();
	}

	@Override
	public abstract float price();
}
