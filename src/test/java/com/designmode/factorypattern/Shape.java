package com.designmode.factorypattern;

public interface Shape {
	void draw();
}
