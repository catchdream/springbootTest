package com.designmode.decoratorpattern;

/**
 * 创建实现Shape接口的抽象装饰器类。
 * 
 * @author tao
 *
 */
public abstract class ShapeDecorator implements Shape {
	protected Shape decoratedShape;

	public ShapeDecorator(Shape decoratedShape) {
		this.decoratedShape = decoratedShape;
	}

	public void draw() {
		decoratedShape.draw();
	}
}
