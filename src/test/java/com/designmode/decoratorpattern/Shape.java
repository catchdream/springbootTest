package com.designmode.decoratorpattern;

public interface Shape {
	void draw();
}
