package com.designmode.abstractfactory;

import com.designmode.factorypattern.Shape;

/**
 * 抽象工厂
 * 
 * @author tao
 *
 */
public abstract class AbstractFactory {
	abstract Color getColor(String color);

	abstract Shape getShape(String shape);
}
