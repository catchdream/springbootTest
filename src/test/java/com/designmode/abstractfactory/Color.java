package com.designmode.abstractfactory;

public interface Color {
	void fill();
}
