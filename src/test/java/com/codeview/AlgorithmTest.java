package com.codeview;

import java.util.Calendar;

/**
 * 算法测试
 * 
 * @author tao
 *
 */
public class AlgorithmTest {

	public static void main(String[] args) {

		int[] a = { 49, 38, 65, 97, 176, 213, 227, 49, 78, 34, 12, 164, 11, 18, 1 };

		// bubbleSort(a);
		Calendar ca = Calendar.getInstance();
		// bubbleSort(a);
		// qSort(a);
		// halfInsertSort(a);
		insertSort(a);

		Calendar ca2 = Calendar.getInstance();
		long timecs = ca2.getTimeInMillis() - ca.getTimeInMillis();
		System.out.println("耗时: " + timecs);

		for (int i = 0; i < a.length; i++)
			System.out.print(a[i] + " ");

	}

	// 冒泡排序
	public static void bubbleSort(int[] a) {
		int len = a.length;

		for (int i = len - 1; i > 0; i--) {
			for (int j = 0; j <= i - 1; j++) {
				if (a[j] > a[j + 1]) {
					swap(a, j, j + 1);
				}
			}
		}

	}

	// 交换位置
	public static void swap(int[] a, int i, int j) {
		int temp = a[i];
		a[i] = a[j];
		a[j] = temp;
	}

	// 快速排序
	public static void qSort(int[] a) {
		if (a == null || a.length <= 1) {
			return;
		}

		partition(a, 0, a.length - 1);
	}

	// 快速排序：递归排序方法
	public static void partition(int[] a, int begin, int end) {
		if (a == null || begin >= end) {
			return;
		}

		int small = begin - 1;

		for (int index = begin; index < end; index++) {
			if (a[index] < a[end]) {
				small++;
				if (index != small) {
					swap(a, index, small);
				}
			}
		}

		small++;
		swap(a, small, end);
		partition(a, begin, small - 1);
		partition(a, small + 1, end);
	}

	// 直接插入排序
	public static void insertSort(int[] a) {
		int len = a.length;
		int temp;
		for (int i = 1; i < len; i++) {
			temp = a[i];
			int j = i - 1;
			while (j >= 0) {
				if (a[j] > temp) {
					a[j + 1] = a[j];
					j--;
				} else {
					break;
				}
			}

			j++;
			a[j] = temp;
		}
	}

	// 二分插入排序
	public static void halfInsertSort(int[] a) {
		for (int i = 0; i < a.length; i++) {
			int temp = a[i];
			int left = 0;
			int right = i - 1;
			int mid = 0;
			while (left <= right) {
				mid = (left + right) / 2;
				if (temp < a[mid]) {
					right = mid - 1;
				} else {
					left = mid + 1;
				}
			}

			for (int j = i - 1; j >= left; j--) {
				a[j + 1] = a[j];
			}

			if (left != i) {
				a[left] = temp;
			}
		}
	}
}
