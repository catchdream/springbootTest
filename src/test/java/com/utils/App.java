package com.utils;

import java.util.Map;

public class App {
	public static void main(String[] args) throws Exception {

		// testAES(content, key);
		// testRSA();
		testMD5();
	}

	public static void testMD5() throws Exception {
		String data = "abcd22236464";
		byte[] md5data = MD5Util.encodeMD5(data);
		System.out.println("md5 加密后：" + new String(md5data));
	}

	public static void testAES() throws Exception {
		// AES
		String content = "abc123,你好！";
		String key = "1234646131sdafa3";
		String s1 = AESUtil.encrypt(content, key);
		System.out.println("AES ENCODE:" + s1);
		String s2 = AESUtil.decrypt(s1, key);
		System.out.println("AES DECODE:" + s2);
	}

	/**
	 * rsa
	 * 
	 * @throws Exception
	 */
	public static void testRSA() throws Exception {
		Map<String, Object> keyMap = RSAUtils.genKeyPair();
		String publicKey = RSAUtils.getPublicKey(keyMap);
		String privateKey = RSAUtils.getPrivateKey(keyMap);
		System.err.println("公钥: \n\r" + publicKey);
		System.err.println("私钥： \n\r" + privateKey);

		// 加密、解密
		System.err.println("公钥加密——私钥解密");
		String source = "这是一行没有任何意义的文字，你看完了等于没看，不是吗？";
		System.out.println("\r加密前文字：\r\n" + source);
		byte[] data = source.getBytes();
		byte[] encodedData = RSAUtils.encryptByPublicKey(data, publicKey);
		System.out.println("加密后文字：\r\n" + new String(encodedData));
		byte[] decodedData = RSAUtils.decryptByPrivateKey(encodedData, privateKey);
		String target = new String(decodedData);
		System.out.println("解密后文字: \r\n" + target);

		// 信息校验
		testSign(privateKey, publicKey);
	}

	/**
	 * rsa sign
	 * 
	 * @param privateKey
	 * @param publicKey
	 * @throws Exception
	 */
	public static void testSign(String privateKey, String publicKey) throws Exception {
		System.err.println("私钥加密——公钥解密");
		String source = "这是一行测试RSA数字签名的无意义文字";
		System.out.println("原文字：\r\n" + source);
		byte[] data = source.getBytes();
		byte[] encodedData = RSAUtils.encryptByPrivateKey(data, privateKey);
		System.out.println("加密后：\r\n" + new String(encodedData));
		byte[] decodedData = RSAUtils.decryptByPublicKey(encodedData, publicKey);
		String target = new String(decodedData);
		System.out.println("解密后: \r\n" + target);
		System.err.println("私钥签名——公钥验证签名");
		String sign = RSAUtils.sign(encodedData, privateKey);
		System.err.println("签名:\r" + sign);
		boolean status = RSAUtils.verify(encodedData, publicKey, sign);
		System.err.println("验证结果:\r" + status);
	}
}
