package com.demo.schedule;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Java Cron表达式用于配置CronTrigger的实例，它是org.quartz.Trigger的子类 \
 * 要让此类生效，需要在DemoApplication.class中加上@EnableScheduling
 * 
 * @author tao
 *
 */
// @Component
public class mySchedule {
	/**
	 * corn表达式，定时任务
	 */
	// @Scheduled(cron = "0 * 9 * * ?")
	public void cronJobSch() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		Date now = new Date();
		String strDate = sdf.format(now);
		System.out.println("Java cron job expression:: " + strDate);
	}

	/**
	 * 固定速率：固定速率调度程序用于在特定时间执行任务 \ 它不等待前一个任务的完成。 值是以毫秒为单位
	 */
	// @Scheduled(fixedRate = 1000)
	public void fixedRateSch() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

		Date now = new Date();
		String strDate = sdf.format(now);
		System.out.println("Fixed Rate scheduler:: " + strDate);
	}

	/**
	 * 固定延迟调度程序用于在特定时间执行任务。 它应该等待上一个任务完成。 值应以毫秒为单位
	 */
	// @Scheduled(fixedDelay = 1000, initialDelay = 3000)
	public void fixedDelaySch() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		Date now = new Date();
		String strDate = sdf.format(now);
		System.out.println("Fixed Delay scheduler:: " + strDate);
	}
}
