package com.demo.exception;

/**
 * 此处，在更新产品时，如果找不到产品，则将响应错误消息返回为:"ProductNotFoundException"
 * 
 * @author tao
 *
 */
public class ProductNotfoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
