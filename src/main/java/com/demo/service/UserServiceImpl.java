package com.demo.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.demo.dao.UserDao;
import com.demo.entity.User;

@Service("userService")
public class UserServiceImpl implements UserService {

	@Resource
	private UserDao userDao;

	public User getUserById(int userId) {
		return userDao.selectByPrimaryKey(userId);
	}

	public boolean addUser(User record) {
		boolean result = false;
		try {
			userDao.insertSelective(record);
			result = true;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

}
