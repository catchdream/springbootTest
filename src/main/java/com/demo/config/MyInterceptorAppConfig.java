package com.demo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.demo.Interceptor.ProductServiceInterceptor;

/**
 * 必须使用WebMvcConfigurerAdapter向InterceptorRegistry注册此Interceptor
 * 
 * @author tao
 *
 */
@Component
public class MyInterceptorAppConfig extends WebMvcConfigurerAdapter {
	@Autowired
	ProductServiceInterceptor productServiceInterceptor;

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		// 配置多个拦截器
		registry.addInterceptor(productServiceInterceptor).addPathPatterns("/products");
		// registry.addInterceptor(productServiceInterceptor).addPathPatterns("/products");
		super.addInterceptors(registry);
	}
}
