package com.demo.config;

import javax.servlet.MultipartConfigElement;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;

/**
 * 未使用：文件上传配置： 使用application.properties配置文件来配置
 * 
 * @author tao
 *
 */
// @Configuration
public class FileUploadConfiguration {
	@Value("${file.resource.path}")
	private String filepath;

	/**
	 * 
	 * @return
	 */
	@Bean
	public MultipartConfigElement multipartConfigElement() {
		MultipartConfigFactory factory = new MultipartConfigFactory();
		// 设置文件大小限制 ,超出设置页面会抛出异常信息，
		// 这样在文件上传的地方就需要进行异常信息的处理了;
		factory.setMaxFileSize("3MB"); // KB,MB
		/// 设置总上传数据总大小
		factory.setMaxRequestSize("20MB");
		// Sets the directory location where files will be stored.
		// factory.setLocation(filepath); // 这个配置没用
		return factory.createMultipartConfig();
	}
}
