package com.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * websocket页面
 * 
 * @author tao
 *
 */
@Controller
public class WebSocketViewController {
	/**
	 * websocket页面
	 * 
	 * @return
	 */
	@RequestMapping(value = "/websocketView", method = RequestMethod.GET)
	public String index() {
		return "websocketTest";
	}
}
