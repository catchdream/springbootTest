package com.demo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 微服务日志跟踪demo
 * 
 * @author tao
 *
 */
@RestController
public class SleuthappTestController {
	// private static final Logger LOG =
	// Logger.getLogger(SleuthappTestController.class.getName());

	@RequestMapping("/testlog")
	public String index() {
		// LOG.log(Level.INFO, "Index API is calling");
		return "Welcome Sleuth!";
	}
}
