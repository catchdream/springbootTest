package com.demo.controller;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.entity.User;
import com.demo.service.RedisUserService;

@RestController
@RequestMapping("rest_redis")
public class RedisController {
	@Resource
	private RedisUserService redisUserService;

	@GetMapping("set")
	public void set() {
		redisUserService.set("key1", new User(1, "meepoguan", 26));
	}

	@GetMapping("get")
	public String get() {
		return redisUserService.get("key1").getUserName();
	}

	@GetMapping("stringset")
	public void stringset() {
		redisUserService.setCode("stringkey", "meepoguan_coke");
	}

	@GetMapping("stringget")
	public String stringget() {
		return redisUserService.getCode("stringkey");
	}
}
